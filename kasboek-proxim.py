import time
import datetime
import os
import RPi.GPIO as GPIO
import json

# read SPI data from MCP3008 chip, 8 possible adc's (0 thru 7)
def readadc(adcnum, clockpin, mosipin, misopin, cspin):
        if ((adcnum > 7) or (adcnum < 0)):
                return -1
        GPIO.output(cspin, True)

        GPIO.output(clockpin, False)  # start clock low
        GPIO.output(cspin, False)     # bring CS low

        commandout = adcnum
        commandout |= 0x18  # start bit + single-ended bit
        commandout <<= 3    # we only need to send 5 bits here
        for i in range(5):
                if (commandout & 0x80):
                        GPIO.output(mosipin, True)
                else:
                        GPIO.output(mosipin, False)
                commandout <<= 1
                GPIO.output(clockpin, True)
                GPIO.output(clockpin, False)

        adcout = 0
        # read in one empty bit, one null bit and 10 ADC bits
        for i in range(12):
                GPIO.output(clockpin, True)
                GPIO.output(clockpin, False)
                adcout <<= 1
                if (GPIO.input(misopin)):
                        adcout |= 0x1

        GPIO.output(cspin, True)

        adcout >>= 1       # first bit is 'null' so drop it
        return adcout


from kasboekfunctions import *
from kasboekprinter import *

## DEBUG ##
DEBUG = 0

## GPIO MODE ##
GPIO.setmode(GPIO.BCM)


## SPI PORTS ##
# change these as desired - they're the pins connected from the
# SPI port on the ADC to the Cobbler
SPICLK = 18
SPIMISO = 23
SPIMOSI = 24
SPICS = 25

## GPIO SETUP ##
# set up the SPI interface pins
GPIO.setup(SPIMOSI, GPIO.OUT)       #??
GPIO.setup(SPIMISO, GPIO.IN)        #??
GPIO.setup(SPICLK, GPIO.OUT)        #??
GPIO.setup(SPICS, GPIO.OUT)         #??
#GPIO.setup(17, GPIO.OUT) piepertje



#############################
## DEFAULT SETTINGS ##


# Mproc Distance
# 130   150
# 143   140
# 158   130
# 168   120
# 180   110
# 200   100
# 216   90
# 248   80
# 272   70
# 322   60
# 373   50
# 460   40
# 590   30
# 770   20


# IR meter connected to adc channel #0
distancemeter_adc = 0;

last_read = 0       # this keeps track of the last distance value
tolerance = 0       # to keep from being jittery we'll only change
                    # distance when the IR has measured more than 5 cm

max_proc = 130
min_proc = 125

sleep = 0.5  # 0.01

max_afstand_nieuwe_bezoeker = 130 # 150 cm of minder. Mproc moet dus groter dan dit getal zijn om een bezoeker te tellen
mproc_voor_ver_weg = 125 # dit of minder dan dit


settings_refresh_rate = 300;
last_settings_refresh = time.time();

update_visitors_refresh_rate = 300 # in sec dus 5 min
last_update_visitors_refresh = 0 #sec

twitter_refresh_rate = 3600
last_twitter_refresh = time.time()

print_value_refresh_rate = 300
last_print_value_refresh = time.time()

print_visitors_refresh_rate = 60
last_print_visitors_refresh = time.time()

print_sold_items_refresh_rate = 1800
last_print_sold_items_refresh = time.time()


print_visitors_counter = 0;


total_value_art = 0 # total value of museum ( on server )

add_value_art = 0 # amount of value to add

last_value_art = 0 # last value of museum since print of value museum

total_number_visitors = 0 # total number of visitors ( on server )

add_number_visitors = 0 # number of vistors to to add


total_value_sold_items = 0 # total value of sold items (on server )

add_value_sold_items = 0 # value of sold itmes to add ## not used ##

last_value_sold_items = 0 # last value since print of sold items


##### GET SETTINGS #########
# get settings from servr
settings = get_settings()

if ( settings['settings_refresh_rate'] > 60 and settings['settings_refresh_rate'] <= 300 ) :

    settings_refresh_rate = float( settings['settings_refresh_rate'] )


if ( settings['update_visitors_refresh_rate'] != '' ) :

    update_visitors_refresh_rate = float( settings['update_visitors_refresh_rate'] )


if ( settings['twitter_refresh_rate'] ) :

    twitter_refresh_rate = float( settings['twitter_refresh_rate'] )


if ( settings['print_value_refresh_rate'] ) :

    print_value_refresh_rate = float( settings['print_value_refresh_rate'] )


if ( settings['print_visitors_refresh_rate'] ) :

    print_visitors_refresh_rate = float( settings['print_visitors_refresh_rate'] )


if ( settings['print_sold_items_refresh_rate'] ) :

    print_sold_items_refresh_rate = float( settings['print_sold_items_refresh_rate'] )



current = get_current()

if ( current['total_value_art'] ) :

    total_value_art = float( current['total_value_art'] )


if ( current['total_number_visitors'] ) :

    total_number_visitors = float( current['total_number_visitors'] )


if ( current['total_value_sold_items'] ) :

    total_value_sold_items = float( current['total_value_sold_items'] )

last_value_sold_items = total_value_sold_items # on init they need to be the same, else our -1 - -2 wont work


print "=== KASBOEK bezoekers teller v0.1 ==="

print "== Current total value museum on server = %d " % total_value_art

print "== Current total visitors count on server = %d " % total_number_visitors

print "== Current total value sold items on server = %d " % total_value_sold_items


try:
    while True:
            # we'll assume that the IR didn't detect since end of last loop
            new_visitor = False

            # get the lastest settings
            clock = time.time()

            print clock


            time_past = clock - last_settings_refresh

            if ( time_past > float( settings_refresh_rate ) ) :

                last_settings_refresh = clock

                settings = get_settings()

                if ( float( settings['settings_refresh_rate'] ) > 60 and float( settings['settings_refresh_rate'] ) <= 300 ) :

                    settings_refresh_rate = float( settings['settings_refresh_rate'] )


                if ( settings['update_visitors_refresh_rate'] != '' ) :

                    update_visitors_refresh_rate = float( settings['update_visitors_refresh_rate'] )


                if ( settings['twitter_refresh_rate'] ) :

                    twitter_refresh_rate = float( settings['twitter_refresh_rate'] )


                if ( settings['print_value_refresh_rate'] ) :

                    print_value_refresh_rate = float( settings['print_value_refresh_rate'] )


                if ( settings['print_visitors_refresh_rate'] ) :

                    print_visitors_refresh_rate = float( settings['print_visitors_refresh_rate'] )


                if ( settings['print_sold_items_refresh_rate'] ) :

                    print_sold_items_refresh_rate = float( settings['print_sold_items_refresh_rate'] )

                print "=== settings updated ==="


            # update visitors count on server?
            clock = time.time()

            time_past = clock - last_update_visitors_refresh

            if ( time_past > float( update_visitors_refresh_rate ) ) :

                last_update_visitors_refresh = clock

                if ( add_number_visitors > 0 ) :

                    total_visitors_count = float( push_value_to_server( add_number_visitors ) )

                    if ( total_visitors_count > 0 ) :

                        add_value_art = 0

                        add_number_visitors = 0

                        current = get_current()

                        if ( current['total_value_art'] ) :

                            total_value_art = float( current['total_value_art'] )


                        if ( current['total_number_visitors'] ) :

                            total_number_visitors = float( current['total_number_visitors'] )


                        if ( current['total_value_sold_items'] ) :

                            total_value_sold_items = float( current['total_value_sold_items'] )


                        if ( last_value_sold_items != total_value_sold_items ) :

                            amount_sold_items = last_value_sold_items - total_value_sold_items

                            if ( amount_sold_items > 0 ) :

                                last_value_sold_items = total_value_sold_items

                                print_update_value( "verkoop/verkauf", "-%d" % amount_sold_items )

                                print "=== printed sold amount = %d ===" % amount_sold_items

                                amount_sold_items = 0;


                        current_date = datetime.date.today()

                        print_museum_value( total_value_art, current_date ) # DIT PRINT

                        print "=== printed museum value = %d on  %s ===" % ( total_value_art, current_date )

                print "=== server updated ==="

                print "=== total val art = %d ===" % total_value_art

                print "=== total num visitors = %d ===" % total_number_visitors

                print "=== total val sold items = %d ===" % total_value_sold_items


              #print value of museum
#             clock = time.time()
#
#             time_past = clock - last_print_value_refresh
#
#             if ( time_past > float( print_value_refresh_rate ) and last_value_art != total_value_art ) :
#
#                 last_print_value_refresh = clock
#
#                 last_value_art = total_value_art
#
#                 current_date = datetime.date.today()
#
#                 print_museum_value( total_value_art, current_date ) # DIT PRINT
#
#                 print "=== printed museum value = %d on  %s ===" % ( total_value_art, current_date )


            # tweet
            clock = time.time()

            time_past = clock - last_twitter_refresh

            if ( time_past > float( twitter_refresh_rate ) ) :

                last_twitter_refresh = clock

                push_tweet_to_server()

                print "=== tweet send to sever ==="



            #print visitors coutner
            clock = time.time()

            time_past = clock - last_print_visitors_refresh

            if ( time_past > float( print_visitors_refresh_rate ) and print_visitors_counter > 0) :

                print_update_value( "bezoeker/besucher", "+%d" % print_visitors_counter )

                print "=== printed visitors counter = %d ===" % print_visitors_counter

                print_visitors_counter = 0;

                last_print_visitors_refresh = clock







#             #print sold items
#             clock = time.time()
#
#             time_past = clock - last_print_sold_items_refresh
#
#             if ( time_past > float( print_sold_items_refresh_rate ) and last_value_sold_items != total_value_sold_items ) :
#
#                 last_print_sold_items_refresh = clock
#
#                 amount_sold_items = last_value_sold_items - total_value_sold_items
#
#                 if ( amount_sold_items > 0 ) :
#
#                     last_value_sold_items = total_value_sold_items
#
#                     print_update_value( "verkoop/verkauf", "-%d" % amount_sold_items )
#
#                     print "=== printed sold amount = %d ===" % amount_sold_items
#
#                     amount_sold_items = 0;
#



            # read the analog pin

            distance = readadc(distancemeter_adc, SPICLK, SPIMOSI, SPIMISO, SPICS) #geeft afstand in 'units': output Voltage door ADC omgezet naar range 0 tot 1024

            # how much has it changed since the last read?

            distance_adjust = abs(distance - last_read)

            v = ( distance / 1023.0 ) * 3.3

            cm_v = 57.9 * v**-1.137 # bereken cm's van volts

            cm_d = 36763 * distance**-1.118 # bereken cm's van microprocessor 'units'

            cm_d = round(cm_d, 2)

            v = round(v, 2)

            if DEBUG:
                    print "distance:", distance

                    print "VOLTS: ", v

                    print "CM: ", cm_d

                    print "distance_adjust:", distance_adjust

                    print "last_read", last_read



            if ( distance > max_afstand_nieuwe_bezoeker and last_read < mproc_voor_ver_weg and distance_adjust > 100 ):

                    # we have a new visitor! - update total visitor variable

                    add_value_art = add_value_art + 1

                    total_value_art = total_value_art + 1

                    total_number_visitors = total_number_visitors + 1

                    add_number_visitors = add_number_visitors + 1

                    print_visitors_counter = print_visitors_counter + 1

                    print "=== Nieuwe bezoeker! ==="

                    print "Number of visitors since last print = %d" % print_visitors_counter



            last_read = distance

            #GPIO.output(17, True) piepertje

            time.sleep( sleep )
            #GPIO.output(17, False) piepertje


except KeyboardInterrupt:
    GPIO.cleanup()
